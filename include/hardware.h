#ifndef HEADER_HARDWARE
#define HEADER_HARDWARE

/*
 * Define the constants used to interact with hardware componants.
 */

#define LEFT_MOTOR 65
#define RIGHT_MOTOR 66
#define OBST_MOTOR 67

#define OPEN_OBST_POS 100

/* 
 * We want speed to be positive when we move forward.
 * However, the orientation of the motor is not necessary
 * the same as the robot.
 * This constant is used to correct the direction.
 */
#define SPEED_SIGN -1

/* 
 * The side on with the sonar is.
 * 1 if the sonar is on the right side of the bot
 * -1 if the sonar is on the left side of the bot
 */
#define SONAR_SIDE 1

/* 
 * Dimentions of the robot
 * (in mm)
 */
#define TRACK 165.
#define WHEEL_CIRCONF 178.
#define SONARD_TO_CENTRAL_AXE 80.
#define TOUCH_SENSOR_TO_WHEEL_AXE 230.

/*
 * The number of turn needed to make a 360 turn by
 * rotating the wheels in oposite direction.
 * = PI*TRACK/WHEEL_CIRCONF
 * There theorical value is 2.91215,
 * but reality sucks. Mesurements tell 3.6, and 
 * I can't be more precise because when I turn to
 * much (~5turns) one motors stop before the other
 * an the end of the turn is screwed.
 */
#define ROTATIONS_FOR_360 5
// #define ROTATIONS_FOR_360 3.75

#endif
