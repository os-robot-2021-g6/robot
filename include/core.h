#ifndef HEADER_RUN
#define HEADER_RUN

// Just in case, add 1 lap
#define NB_LAPS 3
#define DIST_BACKWARD_AFTER_OBSTACE 200.
#define DIST_WALL_SMALL 100.
#define DIST_WALL_MEDIUM 200.
#define DIST_WALL_BIG 450.

/*
 * Headers for core code
 */

/*
 * The main function.
 */
int run();

/*
 * Turn when the robot is in a corner.
 */
void turn_in_corner();

/*
 * Set position to starting position P1
 */
void set_pos_p1();

/*
 * Set position to starting position P2
 */
void set_pos_p2();

/*
 * Set position to starting position P3
 */
void set_pos_p3();

/*
 * Set if the bot is allone in the arena.
 */
void set_is_alone(int is_alone_p);

/*
 * Test if the bot is allone in the arena.
 */
int get_is_alone();

/*
 * Heurostic that estimate if the distance should to the wall
 * should be followed by the feedback loop, or if the robot
 * should just move straight foward.
 */
int test_ignore_mesured_distance(float distance);

/*
 * Hook for bookkeeping and other stuff in the feedback loop.
 */
void hook(void);

/*
 * Condition on which the robot should stop following the wall.
 * Detect if we are in the corner SE so that we can turn close to
 * the fence.
 */
int test_stop_following(void);

#endif
