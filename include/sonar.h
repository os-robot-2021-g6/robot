#ifndef HEADER_SONAR
#define HEADER_SONAR

/*
 * Headers for sonar related features.
 */

/*
 * Get the distance to the wall in mm.
 * Put the distance in the distance,
 * return non-0 value in case of error.
 *
 * First implementation by Yash: https://gitlab.eurecom.fr/os-robot-2021-g6/robot/-/blob/b80e061aecb0172d31632196c6b8682da0623b31/robo.c
 * Refactored by Jean-Marie
 */
int sense_sonar(float * distance);

#endif
