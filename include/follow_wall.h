#ifndef HEADER_FOLLOW_WALL
#define HEADER_FOLLOW_WALL

/*
 * Headers for functions to follow a wall.
 *
 * Basically, we use feedback loops to follow a wall.
 * Different functions use different strategies.
 */

/*
 * Follow a wall using the distance captor.
 * This function uses a linear feedback loop.
 * /!\ this function is blocking
 *
 * target_distance is the distance we want to keep between the robot
 * and the wall.
 *
 * test_stop_following() is the function used to test if we should stop
 *    following the wall.
 * test_ignore_dist() is a function that indicate if we should ignore the 
 *    mesured distance to the wall and move straight foward. Set to NULL,
 *    we never ignore the messured distance.
 * hook() is called every iteration for book keeping. It can be used to 
 *    take over the controle if we detect that the bot lost the wall and 
 *    turn in circle for instance (test_stop_following could be used too
 *    for this specific exemple, anyway...). Set to NULL if not needed.
 *
 * return:
 *  - 0: test_stop_following() returned 1
 *  - 1: failled to control the motor/get the max speed
 *  - 2: multiple faillures to read the sonar
 *
 * Implemented by Jean-Marie
 */
int follow_wall_linear_fbl(
  float target_distance,
  int test_stop_following(void),
  int test_ignore_dist(float),
  void hook(void)
);

/*
 * Follow a wall using the distance captor.
 * This function uses a derivative feedback loop.
 * /!\ this function is blocking
 *
 * test_stop_following() is the function used to test if we should stop
 *    following the wall.
 * test_ignore_dist() is a function that indicate if we should ignore the 
 *    mesured distance to the wall and move straight foward. Set to NULL,
 *    we never ignore the messured distance.
 * hook() is called every iteration for book keeping. It can be used to 
 *    take over the controle if we detect that the bot lost the wall and 
 *    turn in circle for instance (test_stop_following could be used too
 *    for this specific exemple, anyway...). Set to NULL if not needed.
 *
 * return:
 *  - 0: test_stop_following() returned 1
 *  - 1: failled to control the motor/get the max speed
 *  - 2: multiple faillures to read the sonar
 *
 * Implemented by Jean-Marie
 */
int follow_wall_derivative_fbl(
  int test_stop_following(void),
  int test_ignore_dist(float),
  void hook(void)
);

/*
 * Follow a wall using the distance captor.
 * This function uses a 2nd order feedback loop (
 * linear + derivative)
 * /!\ this function is blocking
 *
 * target_distance is the distance we want to keep between the robot
 *    and the wall.
 * test_stop_following() is the function used to test if we should stop
 *    following the wall.
 * test_ignore_dist() is a function that indicate if we should ignore the 
 *    mesured distance to the wall and move straight foward. Set to NULL,
 *    we never ignore the messured distance.
 * hook() is called every iteration for book keeping. It can be used to 
 *    take over the controle if we detect that the bot lost the wall and 
 *    turn in circle for instance (test_stop_following could be used too
 *    for this specific exemple, anyway...). Set to NULL if not needed.
 *
 * return:
 *  - 0: test_stop_following() returned 1
 *  - 1: failled to control the motor/get the max speed
 *  - 2: multiple faillures to read the sonar
 *
 * Implemented by Jean-Marie
 */
int follow_wall_2nd_order_fbl(
  float target_distance,
  int test_stop_following(void),
  int test_ignore_dist(float),
  void hook(void)
);

/*
 * Set the distance used for the followinf test_stop_following functions.
 *
 * Implemented by Jean-Marie
 */
void set_max_distance_test_stop_following(float distance);

/*
 * Test if the current distance is > max_distance_test_stop_following.
 * set_max_distance_test_stop_following as to be called before calling 
 * test_stop_following_distance().
 *
 * Implemented by Jean-Marie
 */
int test_stop_following_distance(void);

/*
 * Test if the touch sensor is active.
 */
int test_stop_following_touch(void);

/*
 * Test if the current distance is > max_distance_test_stop_following OR
 * if the touch sensor is activated.
 * set_max_distance_test_stop_following as to be called before calling 
 * test_stop_following_distance_touch().
 *
 * Implemented by Jean-Marie
 */
int test_stop_following_distance_touch(void);

/*
 * Never ignore the distance to the wall in the feedbackloop.
 */
int never_ignore_dist(float distance);

#endif
