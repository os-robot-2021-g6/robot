#ifndef HEADER_TOUCH
#define HEADER_TOUCH

/*
 * Headers for touch sensor related features.
 */

/*
 * Test is the touch sensor is activated.
 * Put the result (0/1) in `touch` variable,
 * return non-0 value in case of error.
 *
 * Implemented by Jean-Marie
 */
int sense_touch(int * touch);

#endif
