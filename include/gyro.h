#ifndef HEADER_GYRO
#define HEADER_GYRO

/*
 * Headers for features related to the gyroscope.
 */

/*
 * Get the current angle.
 *
 * Implemented by Jean-Marie
 */
int sense_angle(float * angle);

#endif
