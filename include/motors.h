#ifndef HEADER_MOTORS
#define HEADER_MOTORS

/*
 * Headers for motor related features.
 */

// Directions:
typedef enum direction{FOWARD, BACKWARD, LEFT, RIGHT} direction_t;

/*
 * Move the robot in straight line.
 * The direction must be FOWARD or BACKWARD.
 *
 * This function is non blocking: it will 
 * return befor the robot stop.
 *
 * First implementation by Yash: https://gitlab.eurecom.fr/os-robot-2021-g6/robot/-/blob/b80e061aecb0172d31632196c6b8682da0623b31/robo.c
 * Refactored by Jean-Marie
 */
int move_straight(direction_t direction);

/*
 * Turn of 'angle' degree, clockwise.
 *
 * First implementation by Yash: https://gitlab.eurecom.fr/os-robot-2021-g6/robot/-/blob/b80e061aecb0172d31632196c6b8682da0623b31/robo.c
 * Refactored by Jean-Marie
 */
int turn(int angle);
int tt_turn(int angle, float tested_constante);

/*
 * Stop the motors.
 *
 * Implemented by Jean-Marie
 */
int stop_motors();

/*
 * Get the maximum speed of the robot.
 * It is the min of the max speed to the 
 * two motors.
 * The result is storred in max_speed,
 * the return value is non-0 if an error
 * happened.
 *
 * Implemented by Jean-Marie
 */
int get_max_speed(int * max_speed);

/*
 * Set the speed of the motor.
 * /!\ The speed has to be < max_speed, and
 * this is not checked by the function.
 * 
 * Implemented by Jean-Marie
 */
int set_motor_speed(int motor, int speed);

/*
 * Print the motor ports.
 *
 * Written by Jean-Marie
 */
void display_motor_ports();

/*
 * Get the traveled distance in mm.
 */
int get_travel_dist(float * dist);

/*
 * Reset motors.
 */
int reset_motors();

/*
 * Release the obstacle.
 * "R4, prepare to jettison the spare part canisters."
 * https://osproject.deso-palaiseau.fr/img/jettison_spare_parts.gif
 * Note: can I release magnets? should be fun
 *   but not so much if the magnet stick to the
 *   ev3 brick.
 */
int release_obstacle();

/*
 * Put back the obstacle container.
 */
int replace_obstacle_container();

#endif
