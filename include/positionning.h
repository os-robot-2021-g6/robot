#ifndef HEADER_POSITION
#define HEADER_POSITION

/*
 * Headers positionning the robot on the arena.
 * All values are in mm.
 *
 * P1, P2, P3 are starting position
 *
 *                    |---  X_MAX  --->
 *            ---    +-----------------+ 
 *    Y_FENCE ___|   |                 |   ^
 *                   |     ++|         |   |
 *            PL2 ---|--> +++| _ _ _ _ | <-- starting line
 *                   |     ++|         |   |
 *                   |       | P1  P2  |
 *                   |   P3  |         |  Y_MAX
 * starting line --> | _ _ _ |         | 
 *                   |++     |         |   |
 *           PL1 --> |+++    |         |   |
 *            ___    |++     |         |   |
 *      Y_FENCE |    |                 |   |
 *            ---    +-----------------+  ---
 *                 (0,0)
 *                   |------>
 *                     X_FENCE
 *
 *  Plots ( `+` on the map) are centered on (X_PL1, Y_PL1) and (X_PL2, Y_PL2), and 
 *  have a radius of RADIUS_PLOT
 *
 */

#define X_MAX 1200.
#define Y_MAX 2000.
#define Y_FENCE 450.
#define X_FENCE 500.

#define X_PL1 0.
#define Y_PL1 520.
#define X_PL2 500.
#define Y_PL2 1480.
#define RADIUS_PLOT 140.

#define MAP_X_SCALE 50
#define MAP_Y_SCALE 100
#define MAP_X_MAX 27
#define MAP_Y_MAX 22

/*
 * Locations of the map
 */
typedef enum location{
  NE_CORNER,      // 0
  NW_CORNER,      // 1
  SW_CORNER,      // 2
  SE_CORNER,      // 3
  PL_1,           // 4
  PL_2,           // 5
  E_REGION,       // 6
  W_REGION,       // 7
  LOCATION_ERROR  // 8
} location_t;     // 9

/*
 * Orientation of the bot
 */
typedef enum orientation{
  FACING_NORTH,
  FACING_SOUTH,
  FACING_WEST,
  FACING_EAST,
  ORIENTATION_ERROR
} orientation_t;

/*
 * Display a map of the arena.
 */
void display_map();

/*
 * Set the position of the robot.
 */
void set_pos(float x, float y);

/*
 * Get the position in mm.
 */
int get_pos(float * x, float * y);

/*
 * Set the direction of the robot.
 * (x, y) is a unitary direction vector.
 */
void set_direction(float x, float y);

/*
 * Get the direction vector.
 */
int get_direction(float * x, float * y);

/*
 * Get the estimated location of the 
 * robot. The return value is a region
 * (location_t), or LOCATION_ERROR.
 */
location_t get_location();

/*
 * Get the orientation of the bot.
 */
orientation_t get_orientation();

/*
 * Return the expected distance to the wall.
 */
float get_expected_distance();

#endif
