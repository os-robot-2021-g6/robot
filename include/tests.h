#ifndef HEADER_TESTS
#define HEADER_TESTS

/*
 * Test the motors
 */
void test_motors();

/*
 * Test turning
 */
void test_turn();

/*
 * Test the gyroscope.
 */ 
void test_gyro();

/*
 * Test the sonar.
 */
void test_sonar();

/*
 * Test the touche sensor.
 */
void test_touch();

/*
 * Test travel distance computation.
 */
void test_travel_dist();

/*
 * Test the release of obstacle.
 */
void test_obst();

#endif
