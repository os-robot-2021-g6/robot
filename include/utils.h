#ifndef HEADER_UTILS
#define HEADER_UTILS

#include <unistd.h>
#include <time.h>

/*
 * Headers for utility functions
 */

#define MIN(X, Y) (((X) < (Y)) ? (X) : (Y))
#define MAX(X, Y) (((X) > (Y)) ? (X) : (Y))

#define Sleep( msec ) usleep(( msec ) * 1000 )

#define SQRT2_OVER_2 0.7071067811865476

/*
 * Compute the difference in millisec between t0 and t1.
 *
 * Implemented by Jean-Marie
 */
float timedifference_msec(struct timespec t0, struct timespec t1);

/*
 * Normalize an angle.
 * Return the angle in [-180, 180]
 */
int normalize_angle(int angle);

#endif
