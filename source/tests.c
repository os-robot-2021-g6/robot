#include <stdio.h>

#include "tests.h"

#include "gyro.h"
#include "motors.h"
#include "sonar.h"
#include "touch.h"
#include "utils.h"

void test_motors(){
  printf("[--------- TEST MOTORS ---------]\n");
  display_motor_ports();
  printf("[-] Move foward\n");
  move_straight(FOWARD);
  Sleep(1000);
  printf("[-] Move backward\n");
  move_straight(BACKWARD);
  Sleep(1000);
  printf("[-] Stop motors\n");
  stop_motors();
  printf("[-] Move 90° clockwise\n");
  turn(90);
  printf("[--------- TEST MOTORS ---------]\n");
}

void test_turn(){
  printf("[--------- TEST TURNS  ---------]\n");
  turn(90);
  Sleep(1000);
  turn(90);
  Sleep(1000);
  turn(180);
  Sleep(1000);
  turn(-90);
  Sleep(1000);
  turn(-90);
  Sleep(1000);
  turn(-180);
  printf("[--------- TEST TURNS  ---------]\n");
}

void test_gyro(){
  printf("[--------- TEST GYRO ---------]\n");
  float angle;
  int i;
  for (i=0; i<10; i++){
    sense_angle(&angle);
    printf("[-] angle: %f\n", angle);
    Sleep(1000);
  }
  printf("[--------- TEST GYRO ---------]\n");
}

void test_sonar(){
  printf("[-------- TEST SONAR  --------]\n");
  float distance;
  int i;
  for (i=0; i<10; i++){
    sense_sonar(&distance);
    printf("[-] distance: %f\n", distance);
    Sleep(1000);
  }
  printf("[-------- TEST SONAR  --------]\n");
}

void test_touch(){
  printf("[-------- TEST TOUCH  --------]\n");
  int touch;
  int i;
  for (i=0; i<10; i++){
    sense_touch(&touch);
    if (touch)
      printf("[-] contact: yes\n");
    else
      printf("[-] contact: no\n");
    Sleep(1000);
  }
  printf("[-------- TEST TOUCH  --------]\n");
}

void test_travel_dist(){
  printf("[-------- TEST TRAVEL --------]\n");
  float dist;
  get_travel_dist(&dist);
  printf("[-] travel dist: %fcm\n", dist);
  move_straight(FOWARD);
  Sleep(1000);
  get_travel_dist(&dist);
  printf("[-] travel dist: %fcm\n", dist);
  Sleep(1000);
  get_travel_dist(&dist);
  printf("[-] travel dist: %fcm\n", dist);
  stop_motors();
  get_travel_dist(&dist);
  printf("[-] travel dist: %fcm\n", dist);
  move_straight(BACKWARD);
  Sleep(1000);
  stop_motors();
  get_travel_dist(&dist);
  printf("[-] travel dist: %fcm\n", dist);
  printf("[-------- TEST TRAVEL --------]\n");
}

void test_obst(){
  release_obstacle();
  Sleep(1000);
  replace_obstacle_container();
}
