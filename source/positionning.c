
#include <stdio.h>
#include <string.h>

#include "positionning.h"

#include "utils.h"
#include "hardware.h"
#include "motors.h"

const char map[MAP_Y_MAX][MAP_X_MAX] = {
  "+------------------------+",
  "|                        |",
  "|                        |",
  "|                        |",
  "|                        |",
  "|        ++|             |",
  "|       +++|             |",
  "|        ++|             |",
  "|          |- - - - - - -|",
  "|          |             |",
  "|          |             |",
  "|          |             |",
  "|          |             |",
  "|- - - - - |             |",
  "|++        |             |",
  "|+++       |             |",
  "|++        |             |",
  "|                        |",
  "|                        |",
  "|                        |",
  "|                        |",
  "+------------------------+"
};


float last_absolute_pos_x;
float last_absolute_pos_y;

void set_pos(float x, float y){
  last_absolute_pos_x = x;
  last_absolute_pos_y = y;
}

int get_pos(float * x, float * y){
  float dist, dir_x, dir_y;
  if (get_travel_dist(&dist)){
    *x = last_absolute_pos_x;
    *y = last_absolute_pos_y;
    return 1;
  }
  get_direction(&dir_x, &dir_y);
  *x = last_absolute_pos_x + dir_x * dist;
  *y = last_absolute_pos_y + dir_y * dist;
  return 0;
}

float direction_x;
float direction_y;

void set_direction(float x, float y){
  direction_x = x;
  direction_y = y;
}

int get_direction(float * x, float * y){
  // Here we are more or less supposed to use
  // the gyroscope, but, erf, the values are screwed,
  // and it works-ish for now.
  *x = direction_x;
  *y = direction_y;
  return 0;
}

void display_map(){
  float pos_x, pos_y;
  int i, x, y;
  char buffer[MAP_X_MAX];

  get_pos(&pos_x, &pos_y);

  x = (int)(pos_x/MAP_X_SCALE+0.5) + 1;
  x = MAX(0, x);
  x = MIN(x, MAP_X_MAX-1);
  y = MAP_Y_MAX - (int)(pos_y/MAP_Y_SCALE+0.5) - 1;

  for (i=0; i<MAP_Y_MAX; i++){
    strcpy(buffer, map[i]);
    if (y == i)
      buffer[x] = 'X';
    printf("%s\n", buffer);
  }
  #ifdef DEBUD
  printf("pos: %f, %f\n", pos_x/10, pos_y/10);
  printf("map x,y: %d, %d\n", x, y);
  #endif
}

location_t get_location(){
  float x, y;
  if (get_pos(&x, &y))
    return LOCATION_ERROR;
  #ifdef DEBUG
  printf("pos: (%f, %f)\n", x, y);
  #endif
  if (x < X_FENCE){
    if ((y > Y_PL1 - RADIUS_PLOT) &&
        (y < Y_PL1 + RADIUS_PLOT + 100.)){
      #ifdef DEBUG
      printf("PLOT 1\n");
      #endif
      return PL_1;
    }
    if ((y > Y_PL2 - RADIUS_PLOT) &&
        (y < Y_PL2 + RADIUS_PLOT + 100.)){
      #ifdef DEBUG
      printf("PLOT 2\n");
      #endif
      return PL_2;
    }
  }
  if (y < Y_FENCE + 100.){
    if (x < X_FENCE){
      #ifdef DEBUG
      printf("SW CORNER\n");
      #endif
      return SW_CORNER;
    }else{
      #ifdef DEBUG
      printf("SE CORNER\n");
      #endif
      return SE_CORNER;
    }
  }
  if (y > Y_MAX - Y_FENCE-100.){
    if (x < X_FENCE){
      #ifdef DEBUG
      printf("NW CORNER\n");
      #endif
      return NW_CORNER;
    }else{
      #ifdef DEBUG
      printf("NE CORNER\n");
      #endif
      return NE_CORNER;
    }
  }
  if (x < X_FENCE){
    #ifdef DEBUG
    printf("WEST\n");
    #endif
    return W_REGION;
  }else{
    #ifdef DEBUG
    printf("EAST\n");
    #endif
    return E_REGION;
  }
}

orientation_t get_orientation(){
  float o_x, o_y;
  get_direction(&o_x, &o_y);
  if (o_x >= SQRT2_OVER_2)
    return FACING_EAST;
  if (o_x <= -SQRT2_OVER_2)
    return FACING_WEST;
  if (o_y >= SQRT2_OVER_2)
    return FACING_NORTH;
  if (o_y <= -SQRT2_OVER_2)
    return FACING_SOUTH;
  return ORIENTATION_ERROR;
}

float get_expected_distance(){
  float x, y;
  orientation_t orientation;
  get_pos(&x, &y);
  orientation = get_orientation();
  if (orientation == FACING_EAST){
    return y - SONARD_TO_CENTRAL_AXE;
  }
  if (orientation == FACING_WEST){
    return Y_MAX - y - SONARD_TO_CENTRAL_AXE;
  }
  if (
    (orientation == FACING_NORTH) &&
    (y > Y_FENCE) &&
    (y < Y_MAX - Y_FENCE) && 
    (x < X_FENCE)
  ){
    return X_FENCE - x - SONARD_TO_CENTRAL_AXE;
  }
  if (orientation == FACING_NORTH){
    return X_MAX - x - SONARD_TO_CENTRAL_AXE;
  }
  if (
    (orientation == FACING_SOUTH) &&
    (y > Y_FENCE) &&
    (y < Y_MAX - Y_FENCE) && 
    (x > X_FENCE)
  ){
    return x - X_FENCE - SONARD_TO_CENTRAL_AXE;
  }
  if (orientation == FACING_SOUTH){
    return x - SONARD_TO_CENTRAL_AXE;
  }

  return -1.;
}
