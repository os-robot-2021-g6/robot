#ifdef DEBUG
#include <stdio.h>
#endif
#include <time.h>

#include "follow_wall.h"

#include "sonar.h"
#include "touch.h"
#include "motors.h"
#include "hardware.h"
#include "utils.h"
#include "positionning.h"

#define MAX_SPEED_FACTOR 1

#define LINEAR_FBL_K_P 6.
#define LINEAR_FBL_LOOSENESS_P 1.

#define DERIVATIVE_FBL_K_D 2500.
#define DERIVATIVE_FBL_LOOSENESS_D 0.

#define ORDER2_FBL_K_P 6.
#define ORDER2_FBL_LOOSENESS_P 1.
#define ORDER2_FBL_K_D 0
#define ORDER2_FBL_LOOSENESS_D 1.

int follow_wall_linear_fbl(
  float target_distance,
  int test_stop_following(void),
  int test_ignore_dist(float),
  void hook(void)
){
  int max_speed, left_speed, right_speed;
  int nb_failled_sonar_mesurements;
  float distance, delta_distance;

  if (test_ignore_dist == NULL)
    test_ignore_dist = never_ignore_dist;

  if (get_max_speed(&max_speed)){
    stop_motors();
    return 1;
  }

  for (;;){
    hook();
    if (test_stop_following()){
      #ifdef DEBUG
      printf("[d] Stop following the wall.\n");
      #endif
      stop_motors();
      return 0;
    }
    
    // Retrieve the distance to the wall.
    // If we fail to retreive it 3 times,
    // stop the motors and end the function.
    nb_failled_sonar_mesurements = 0;
    for (;sense_sonar(&distance);){
      nb_failled_sonar_mesurements++;
      if (nb_failled_sonar_mesurements >= 3){
        #ifdef DEBUG
        printf("[d] Stopped following wall: failled to read the distance 3 times\n");
        #endif
        stop_motors();
        return 2;
      }
    }

    // SONAR_SIDE corrects the sign ot the delta depending on
    // the side of the captor.
    delta_distance = SONAR_SIDE * (distance - target_distance);

    left_speed = max_speed * MAX_SPEED_FACTOR;
    right_speed = max_speed * MAX_SPEED_FACTOR;
    if (!test_ignore_dist(distance)){
      if (delta_distance < -LINEAR_FBL_LOOSENESS_P){
        left_speed += (int)(LINEAR_FBL_K_P*delta_distance);
      }else if (delta_distance > LINEAR_FBL_LOOSENESS_P){
        right_speed -= (int)(LINEAR_FBL_K_P*delta_distance);
      }
    }
    left_speed = MIN(
      max_speed*MAX_SPEED_FACTOR,
      MAX(
        -max_speed*MAX_SPEED_FACTOR,
        left_speed
      )
    );
    right_speed = MIN(
      max_speed*MAX_SPEED_FACTOR,
      MAX(
        -MAX_SPEED_FACTOR*MAX_SPEED_FACTOR,
        right_speed
      )
    );


    #ifdef DEBUG
    printf("[d] Linear feedback loop:\n");
    printf("[+][d] delta_distance = %f\n", delta_distance);
    printf("[+][d] left_speed = %d\n", left_speed);
    printf("[+][d] right_speed = %d\n", right_speed);
    #endif

    if (
        set_motor_speed(LEFT_MOTOR, left_speed) || 
        set_motor_speed(RIGHT_MOTOR, right_speed)
    ){
      #ifdef DEBUG
      printf("[d] Stopped following wall: error while setting the speed\n");
      #endif
      stop_motors(); // try anyway
      return 1;
    }

    Sleep(100);
  }
}

int follow_wall_derivative_fbl(
  int test_stop_following(void),
  int test_ignore_dist(float),
  void hook(void)
){
  int max_speed, left_speed, right_speed;
  int nb_failled_sonar_mesurements;
  float distance, previous_distance, derivative;
  struct timespec date_mesurement, date_previous_mesurement;

  if (test_ignore_dist == NULL)
    test_ignore_dist = never_ignore_dist;

  if (get_max_speed(&max_speed)){
    stop_motors();
    return 1;
  }

  previous_distance = -1.;

  for (;;){
    hook();
    if (test_stop_following()){
      #ifdef DEBUG
      printf("[d] Stop following the wall.\n");
      #endif
      stop_motors();
      return 0;
    }
    
    // Retrieve the distance to the wall.
    // If we fail to retreive it 3 times,
    // stop the motors and end the function.
    nb_failled_sonar_mesurements = 0;
    for (;sense_sonar(&distance);){
      nb_failled_sonar_mesurements++;
      if (nb_failled_sonar_mesurements >= 3){
        #ifdef DEBUG
        printf("[d] Stopped following wall: failled to read the distance 3 times\n");
        #endif
        stop_motors();
        return 2;
      }
    }
    clock_gettime(CLOCK_MONOTONIC, &date_mesurement);

    if (previous_distance < 0.){
      previous_distance = distance;
      date_previous_mesurement = date_mesurement;
      continue;
    }

    // SONAR_SIDE corrects the sign ot the delta depending on
    // the side of the captor.
    derivative = SONAR_SIDE * (distance - previous_distance) / timedifference_msec(date_previous_mesurement, date_mesurement);

    left_speed = max_speed * MAX_SPEED_FACTOR;
    right_speed = max_speed * MAX_SPEED_FACTOR;

    if (!test_ignore_dist(distance)){
      if (derivative < -DERIVATIVE_FBL_LOOSENESS_D){
        left_speed += (int)(DERIVATIVE_FBL_K_D*derivative);
      }else if (derivative > DERIVATIVE_FBL_LOOSENESS_D){
        right_speed -= (int)(DERIVATIVE_FBL_K_D*derivative);
      }
    }

    left_speed = MIN(
      max_speed*MAX_SPEED_FACTOR,
      MAX(
        -max_speed*MAX_SPEED_FACTOR,
        left_speed
      )
    );
    right_speed = MIN(
      max_speed*MAX_SPEED_FACTOR,
      MAX(
        -MAX_SPEED_FACTOR*MAX_SPEED_FACTOR,
        right_speed
      )
    );

    #ifdef DEBUG
    printf("[d] Derivative feedback loop:\n");
    printf("[+][d] derivative = (%f-%f)/%f = %f\n", 
        distance, 
        previous_distance,
        timedifference_msec(date_previous_mesurement, date_mesurement),
        derivative
    );
    printf("[+][d] left_speed = %d\n", left_speed);
    printf("[+][d] right_speed = %d\n", right_speed);
    #endif

    if (
        set_motor_speed(LEFT_MOTOR, left_speed) || 
        set_motor_speed(RIGHT_MOTOR, right_speed)
    ){
      #ifdef DEBUG
      printf("[d] Stopped following wall: error while setting the speed\n");
      #endif
      stop_motors(); // try anyway
      return 1;
    }

    previous_distance = distance;
    date_previous_mesurement = date_mesurement;

    Sleep(10);
  }
}

int follow_wall_2nd_order_fbl(
  float target_distance,
  int test_stop_following(void),
  int test_ignore_dist(float),
  void hook(void)
){
  int max_speed, left_speed, right_speed;
  int nb_failled_sonar_mesurements;
  float distance, delta_distance, previous_distance, derivative;
  struct timespec date_mesurement, date_previous_mesurement;
  int ignore_dist;
  
  if (test_ignore_dist == NULL)
    test_ignore_dist = never_ignore_dist;

  if (get_max_speed(&max_speed)){
    stop_motors();
    return 1;
  }

  previous_distance = -1.;

  for (;;){
    hook();
    if (test_stop_following()){
      #ifdef DEBUG
      printf("[d] Stop following the wall.\n");
      #endif
      stop_motors();
      return 0;
    }
    
    // Retrieve the distance to the wall.
    // If we fail to retreive it 3 times,
    // stop the motors and end the function.
    nb_failled_sonar_mesurements = 0;
    for (;sense_sonar(&distance);){
      nb_failled_sonar_mesurements++;
      if (nb_failled_sonar_mesurements >= 3){
        #ifdef DEBUG
        printf("[d] Stopped following wall: failled to read the distance 3 times\n");
        #endif
        stop_motors();
        return 2;
      }
    }
    clock_gettime(CLOCK_MONOTONIC, &date_mesurement);

    delta_distance = SONAR_SIDE * (distance - target_distance);

    left_speed = max_speed * MAX_SPEED_FACTOR;
    right_speed = max_speed * MAX_SPEED_FACTOR;

    ignore_dist = test_ignore_dist(distance);
    if (!ignore_dist){
      if (delta_distance < -ORDER2_FBL_LOOSENESS_P){
        left_speed += (int)(ORDER2_FBL_K_P*delta_distance);
      }else if (delta_distance > ORDER2_FBL_LOOSENESS_P){
        right_speed -= (int)(ORDER2_FBL_K_P*delta_distance);
      }
    }

    if (previous_distance < 0.){
      derivative = 0.;
    }else{
        // SONAR_SIDE corrects the sign ot the delta depending on
        // the side of the captor.
        derivative = SONAR_SIDE * (distance - previous_distance) / timedifference_msec(date_previous_mesurement, date_mesurement);
    }

    if (!ignore_dist){
      if (derivative < -ORDER2_FBL_LOOSENESS_D){
        left_speed += (int)(ORDER2_FBL_K_D*derivative);
      }else if (derivative > ORDER2_FBL_LOOSENESS_D){
        right_speed -= (int)(ORDER2_FBL_K_D*derivative);
      }
    }

    left_speed = MIN(
      max_speed*MAX_SPEED_FACTOR,
      MAX(
        -max_speed*MAX_SPEED_FACTOR,
        left_speed
      )
    );
    right_speed = MIN(
      max_speed*MAX_SPEED_FACTOR,
      MAX(
        -MAX_SPEED_FACTOR*MAX_SPEED_FACTOR,
        right_speed
      )
    );

    #ifdef DEBUG
    printf("[d] 2nd order feedback loop:\n");
    if (previous_distance < 0.)
      printf("[+][d] first iteraction, derivation undefined\n");
    else
      printf("[+][d] derivative = (%f-%f)/%f = %f\n", 
          distance, 
          previous_distance,
          timedifference_msec(date_previous_mesurement, date_mesurement),
          derivative
      );
    printf("[+][d] delta_distance = %f\n", delta_distance);
    printf("[+][d] left_speed = %d\n", left_speed);
    printf("[+][d] right_speed = %d\n", right_speed);
    #endif

    if (
        set_motor_speed(LEFT_MOTOR, left_speed) || 
        set_motor_speed(RIGHT_MOTOR, right_speed)
    ){
      #ifdef DEBUG
      printf("[d] Stopped following wall: error while setting the speed\n");
      #endif
      stop_motors(); // try anyway
      return 1;
    }

    previous_distance = distance;
    date_previous_mesurement = date_mesurement;

    Sleep(10);
  }
}

float max_distance_test_stop_following;

void set_max_distance_test_stop_following(float distance){
  max_distance_test_stop_following = distance;
}

int test_stop_following_distance(void){
  float distance;
  int nb_failled_sonar_mesurements;

  nb_failled_sonar_mesurements = 0;
  for (;sense_sonar(&distance);){
    nb_failled_sonar_mesurements++;
    if (nb_failled_sonar_mesurements >= 3){
      #ifdef DEBUG
      printf("[d] test_stop_following_distance() failed:\n    failled to read the distance 3 times\n");
      #endif
      return 1;
    }
  }

  if (distance > max_distance_test_stop_following){
    #ifdef DEBUG
    printf("[d] Distance %f > %f: stop following wall\n", distance, max_distance_test_stop_following);
    #endif
    return 1;
  }
  return 0;
}

int test_stop_following_touch(void){
  int touched;
  int nb_failled_touch_mesurements;

  nb_failled_touch_mesurements = 0;
  for (;sense_touch(&touched);){
    nb_failled_touch_mesurements++;
    if (nb_failled_touch_mesurements >= 3){
      #ifdef DEBUG
      printf("[d] test_stop_following_distance_touch() failed:\n    failled to read the touch sensor 3 times\n");
      #endif
      return 1;
    }
  }
  if (touched){
    #ifdef DEBUG
    printf("[d] touched the wall. Stop following wall\n");
    #endif
    return 1;
  }
  return 0;
}

int test_stop_following_distance_touch(void){
  if (test_stop_following_touch())
    return 1;
  if (test_stop_following_distance())
    return 1;
  return 0;
}

int never_ignore_dist(float distance){
  (void)distance; // Remove "unused var" warning
  return 0;
}
