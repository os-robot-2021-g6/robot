#include <stdio.h>

#include "ev3.h"
#include "ev3_port.h"
#include "ev3_tacho.h"

#include "motors.h"
#include "hardware.h"
#include "gyro.h"
#include "utils.h"

// Thats a lot :/
#define ANGLE_LOOSENESS 5


int move_straight(direction_t direction){
  int dir;
  uint8_t sn_left,sn_right;   
  int max_speed;

  if (direction == FOWARD)
    dir = SPEED_SIGN;
  else if (direction == BACKWARD)
    dir = -SPEED_SIGN;
  else{
    printf("[-] move_straight() failed\n    The direction can only be BACKWARD or FOWARD\n");
    return 1;
  }
        
  if (
       ev3_search_tacho_plugged_in(LEFT_MOTOR,0, &sn_left,0) &&
       ev3_search_tacho_plugged_in(RIGHT_MOTOR,0,&sn_right,0)
     ){

     if (get_max_speed(&max_speed)){
       return 1;
     }
    
    set_tacho_speed_sp(sn_left, dir*max_speed * 2/3); 
    set_tacho_speed_sp(sn_right, dir*max_speed * 2/3); 

    set_tacho_command_inx(sn_left, TACHO_RUN_FOREVER); 
    set_tacho_command_inx(sn_right, TACHO_RUN_FOREVER); 

  }else{
    printf("[-] move_straight() failed\n    Tacho motors not plugged in Port %d and %d \n", LEFT_MOTOR, RIGHT_MOTOR);
    return 1;
  }
 return 0;
}

int turn(int angle){
  // This function is has the precision of a jackhammer
  uint8_t sn_right, sn_left;
  int max_speed;
  /*
  int failled_sense_angle;
  int error;
  int speed_factor;
  float angle_before, angle_after;
  */

  //angle = normalize_angle(angle);

  /*
  if ((angle > 90) || (angle < -90)){
    speed_factor = 4;
  }else if ((angle > 45) || (angle < -45)){
    speed_factor = 2;
  }else{
    speed_factor = 1;
  }
  */

  #ifdef DEBUG
  printf("[d] turn(%d)\n", angle);
  //printf("[d] speed_factor = %d/4\n", speed_factor);
  #endif

  if (get_max_speed(&max_speed)){
    printf("[-] turn() failed, failed to get max speed\n");
    return 1;
  }
  if(
    ev3_search_tacho_plugged_in(RIGHT_MOTOR, 0, &sn_right, 0) &&
    ev3_search_tacho_plugged_in(LEFT_MOTOR, 0, &sn_left, 0)
  ){
    //failled_sense_angle = sense_angle(&angle_before);
    set_tacho_stop_action_inx(sn_right, TACHO_HOLD); 
    set_tacho_stop_action_inx(sn_left, TACHO_HOLD); 
    //set_tacho_speed_sp(sn_right, max_speed * 2 * speed_factor / 4 / 3); 
    //set_tacho_speed_sp(sn_left, max_speed * 2 * speed_factor / 4 / 3); 
    set_tacho_speed_sp(sn_right, max_speed * 2 / 3); 
    set_tacho_speed_sp(sn_left, max_speed * 2 / 3); 
    set_tacho_position_sp(sn_right, (int)(angle*ROTATIONS_FOR_360));  // there is a 'x 360/360' simplified here// *15/4 beccause it works... 
    set_tacho_position_sp(sn_left, (int)(-angle*ROTATIONS_FOR_360));   // I'll clean that up, some day, maybe
    set_tacho_command_inx(sn_right, TACHO_RUN_TO_REL_POS); 
    set_tacho_command_inx(sn_left, TACHO_RUN_TO_REL_POS); 
    Sleep(500); //Not very elegent. How do we know for sure the turn is finished?
                // Maybe compute the Sleep time from the angle?
    /*
    failled_sense_angle &= sense_angle(&angle_after);
    if (!failled_sense_angle){
      error = (int)(angle-(angle_after-angle_before));
      #ifdef DEBUG
      printf("[+] Angle error: %d\n", error);
      #endif
      if ((error > ANGLE_LOOSENESS) || (error < -ANGLE_LOOSENESS))
        turn(-error);
    }else{
      printf("[-] failled to read the angle, turning blind\n");
    }
    */

  }else{
    printf("[-] turn() failed\n    Tacho motors not plugged in Port %d and %d \n", LEFT_MOTOR, RIGHT_MOTOR);
    return 1;
  }
  return 0;
}

int tt_turn(int angle, float tested_constante){
  uint8_t sn_right, sn_left;
  int max_speed;
  printf("[d] turn(%d)\n", angle);

  if (get_max_speed(&max_speed)){
    printf("[-] turn() failed, failed to get max speed\n");
    return 1;
  }
  if(
    ev3_search_tacho_plugged_in(RIGHT_MOTOR, 0, &sn_right, 0) &&
    ev3_search_tacho_plugged_in(LEFT_MOTOR, 0, &sn_left, 0)
  ){
    set_tacho_stop_action_inx(sn_right, TACHO_HOLD); 
    set_tacho_stop_action_inx(sn_left, TACHO_HOLD); 
    set_tacho_speed_sp(sn_right, max_speed * 2 / 3); 
    set_tacho_speed_sp(sn_left, max_speed * 2 / 3); 
    set_tacho_position_sp(sn_right, (int)(angle*tested_constante));  
    set_tacho_position_sp(sn_left, (int)(-angle*tested_constante)); 
    set_tacho_command_inx(sn_right, TACHO_RUN_TO_REL_POS); 
    set_tacho_command_inx(sn_left, TACHO_RUN_TO_REL_POS); 
    Sleep(3000); 
  }else{
    printf("[-] turn() failed\n    Tacho motors not plugged in Port %d and %d \n", LEFT_MOTOR, RIGHT_MOTOR);
    return 1;
  }
  return 0;
}

int stop_motors(){
  uint8_t sn_left,sn_right;   
  if (
       ev3_search_tacho_plugged_in(LEFT_MOTOR,0, &sn_left,0) &&
       ev3_search_tacho_plugged_in(RIGHT_MOTOR,0,&sn_right,0)
     ){
    set_tacho_command_inx(sn_left, TACHO_STOP); 
    set_tacho_command_inx(sn_right, TACHO_STOP); 
  }else{
    printf("[-] stop_motors() failed\n    Tacho motors not plugged in Port %d and %d \n", LEFT_MOTOR, RIGHT_MOTOR);
    return 1;
  }
  return 0;
}

int get_max_speed(int * max_speed){
  uint8_t sn_left,sn_right;   
  int max_speed_left, max_speed_right; 
        
  if (
       ev3_search_tacho_plugged_in(LEFT_MOTOR,0, &sn_left,0) &&
       ev3_search_tacho_plugged_in(RIGHT_MOTOR,0,&sn_right,0)
     ){
    if (
      !get_tacho_max_speed(sn_left, &max_speed_left) ||
      !get_tacho_max_speed(sn_right, &max_speed_right)
    ){
      #ifdef DEBUG
      printf("[d] failed to read max_speed\n");
      #endif
      return 1;
    }

    *max_speed = MIN(max_speed_left, max_speed_right);

    #ifdef DEBUG
    printf("[d] Left motor: %d, \t Max Speed: %d\n", LEFT_MOTOR, max_speed_left);
    printf("[d] Right motor: %d, \t Max Speed: %d\n", RIGHT_MOTOR, max_speed_right);
    printf("[d] Max speed: %d\n", *max_speed);
    #endif

  }else{
    printf("[-] get_max_speed() failed\n    Tacho motors not plugged in Port %d and %d \n", LEFT_MOTOR, RIGHT_MOTOR);
    return 1;
  }
 return 0;
}

int set_motor_speed(int motor, int speed){
  uint8_t sn;
  if (ev3_search_tacho_plugged_in(motor, 0, &sn,0)){
    set_tacho_speed_sp(sn, SPEED_SIGN * speed);
    set_tacho_command_inx(sn, TACHO_RUN_FOREVER); 
  }else{
    printf("[-] set_motor_speed(%d, %d) failed\n    Tacho motors not plugged in Port %d\n", motor, speed, motor);
    return 1;
  }
 return 0;
}

void display_motor_ports(){
  int i;
  char s[256];
  for (i = 0; i< 64; i++){
    if (ev3_tacho[i].type_inx != TACHO_TYPE__NONE_){
        printf("[d] Tacho type = %s\n", ev3_tacho_type(ev3_tacho[i].type_inx));
        printf("[d] Tacho Port = %s\n", ev3_tacho_port_name(i,s));
        printf("[d] Tacho Port Number = %d ... %d\n", ev3_tacho_desc_port(i), ev3_tacho_desc_extport(i));
    }
  }
}

int get_travel_dist(float * dist){
  uint8_t sn_left,sn_right;   
  int error_left, error_right;
  int tacho_position_left, tacho_position_right;

  tacho_position_left = 0;
  tacho_position_right = 0;
  
  error_left = !ev3_search_tacho_plugged_in(LEFT_MOTOR,0, &sn_left,0);
  error_right = !ev3_search_tacho_plugged_in(RIGHT_MOTOR,0,&sn_right,0);

  if (!error_left){
    if (get_tacho_position(sn_left, &tacho_position_left) == 0){
      error_left = 1;
      printf("[-] Error while reading distance from left motot\n");
    }
  }else{
    printf("[-] Motor left not found at %d\n", LEFT_MOTOR);
  }
  if (!error_right){
    if (get_tacho_position(sn_right, &tacho_position_right) == 0){
      error_right = 1;
      printf("[-] Error while reading distance from right motot\n");
    }
  }else{
    printf("[-] Motor right not found at %d\n", RIGHT_MOTOR);
  }

  #ifdef DEBUG
  printf("[d] tacho_position_left = %d\n", tacho_position_left);
  printf("[d] tacho_position_right = %d\n", tacho_position_right);
  #endif

  *dist = 0.;
  if (!error_left)
    *dist += SPEED_SIGN*(float)tacho_position_left * WHEEL_CIRCONF / 360.;
  if (!error_right)
    *dist += SPEED_SIGN*(float)tacho_position_right * WHEEL_CIRCONF / 360.;

  if (!error_left && !error_right)
    *dist /= 2.;

  if (error_left && error_right){
    printf("[-] Failed to read distance from both left and right motors\n");
    return 1;
  }
  return 0;
}

int reset_motors(){
  uint8_t sn_left,sn_right;   
  if (
       ev3_search_tacho_plugged_in(LEFT_MOTOR,0, &sn_left,0) &&
       ev3_search_tacho_plugged_in(RIGHT_MOTOR,0,&sn_right,0)
     ){
    set_tacho_command_inx(sn_left, TACHO_RESET);
    set_tacho_command_inx(sn_right, TACHO_RESET);
  }else{
    printf("[-] reset_motors() failed\n    Tacho motors not plugged in Port %d and %d \n", LEFT_MOTOR, RIGHT_MOTOR);
    return 1;
  }
  return 0;
}

int release_obstacle(){
  uint8_t sn;
  int max_speed;
  #ifdef DEBUG
  printf("[d] release obstacle\n");
  #endif
  if (ev3_search_tacho_plugged_in(OBST_MOTOR,0, &sn,0)){
    if (!get_tacho_max_speed(sn, &max_speed)){
      printf("[-] release_obstacle() failed\n    Failed to read max speed\n");
    }
    set_tacho_stop_action_inx(sn, TACHO_HOLD);
    set_tacho_speed_sp(sn, max_speed * 2 / 3);
    set_tacho_position_sp(sn, OPEN_OBST_POS);
    set_tacho_command_inx(sn, TACHO_RUN_TO_ABS_POS);
  }else{
    printf("[-] release_obstacle() failed\n    Tacho motors not plugged in Port %d\n", OBST_MOTOR);
    return 1;
  }
  return 0;
}

int replace_obstacle_container(){
  uint8_t sn;
  int max_speed;
  #ifdef DEBUG
  printf("[d] put the obstacle container back in place\n");
  #endif
  if (ev3_search_tacho_plugged_in(OBST_MOTOR,0, &sn,0)){
    if (!get_tacho_max_speed(sn, &max_speed)){
      printf("[-] replace_obstacle_container() failed\n    Failed to read max speed\n");
    }
    set_tacho_stop_action_inx(sn, TACHO_HOLD);
    set_tacho_speed_sp(sn, -max_speed * 2 / 3);
    set_tacho_position_sp(sn, 0);
    set_tacho_command_inx(sn, TACHO_RUN_TO_ABS_POS);
  }else{
    printf("[-] replace_obstacle_container() failed\n    Tacho motors not plugged in Port %d\n", OBST_MOTOR);
    return 1;
  }
  return 0;
}
