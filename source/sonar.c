#include <stdio.h>

#include "ev3.h"
#include "ev3_sensor.h"

#include "sonar.h"

int sense_sonar(float * distance){
  uint8_t sn_sonar;
  if(ev3_search_sensor(LEGO_EV3_US, &sn_sonar, 0)){
    if(!get_sensor_value0(sn_sonar, distance)){
      printf("[-] sense_sonar() failed\n    get_sensor_value0() return non-0 value\n");
      return 1;
    }else{
      #ifdef DEBUG
      printf("[d] sonar: %f\n", *distance);
      #endif
      return 0;
    }
  }
  printf("[-] sense_sonar() failed\n    Sonar not found\n");
  return 1;
}
