#include <stdio.h>

#include "ev3.h"
#include "ev3_sensor.h"

#include "gyro.h"

int sense_angle(float * angle){
  uint8_t sn_gyro;
  if(ev3_search_sensor(LEGO_EV3_GYRO, &sn_gyro, 0)){
    if(!get_sensor_value0(sn_gyro, angle)){
      printf("[-] sense_angle() failed\n    get_sensor_value0() return non-0 value\n");
      return 1;
    }else{
      #ifdef DEBUG
      printf("[d] angle: %f\n", *angle);
      #endif
      return 0;
    }
  }
  printf("[-] sense_angle() failed\n    Gyroscope not found\n");
  return 1;
}
