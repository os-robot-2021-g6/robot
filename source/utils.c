#include <time.h>

#include "utils.h"

float timedifference_msec(struct timespec t0, struct timespec t1){
  float val;
  val =  (float)(t1.tv_sec - t0.tv_sec) * 1000. + 
         (float)(t1.tv_nsec - t0.tv_nsec) / 1000000.;
  if (val < 0)
    return -val;
  return val;
}

int normalize_angle(int angle){
  if ((angle % 360) > 180)
    return (angle % 360) - 360;
  else if ((angle % 360) < -180)
    return (angle % 360) + 360;
  else
    return angle % 360;
}
