#include <stdio.h>

#include "ev3.h"
#include "ev3_sensor.h"

#include "touch.h"

int sense_touch(int * touch){
  uint8_t sn;
  if (ev3_search_sensor(LEGO_EV3_TOUCH, &sn, 0)){
    if (!get_sensor_value(0, sn, touch)){
      printf("[-] sense_touch() failed\n    get_sensor_value0() return non-0 value\n");
      return 1;
    }
    #ifdef DEBUG
    if (*touch)
      printf("[d] Touch sensor activated\n");
    else
      printf("[d] Touch sensor not activated\n");
    #endif
    return 0;
  }
  printf("[-] sense_touch() failed\n    Touch sensor not found\n");
  return 1;
}
