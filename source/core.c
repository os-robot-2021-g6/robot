#include <stdio.h>
#include <time.h>

#include "core.h"

#include "sonar.h"
#include "positionning.h"
#include "follow_wall.h"
#include "utils.h"
#include "touch.h"
#include "motors.h"

#define DISTANCE_THREESHOLD 100.
int starting_pos;
float target_distance_to_wall;
int hook_frame;
int is_alone;
int current_lap_quarter;
struct timespec date_obst_release, date_last_bump;
int obst_released, obst_container_in_place;

int run(){
  location_t loc, prev_corner;
  int touch, dir;
  float dist;

  prev_corner = LOCATION_ERROR;
  current_lap_quarter = 0;
  hook_frame = 0;
  obst_released = 0;
  obst_container_in_place = 1;


  while (current_lap_quarter <= 4*NB_LAPS){
    follow_wall_2nd_order_fbl(
      target_distance_to_wall,
      test_stop_following,
      test_ignore_mesured_distance,
      hook
    );
    loc = get_location();
    clock_gettime(CLOCK_MONOTONIC, &date_last_bump);
    sense_touch(&touch);
    printf("Loc: %d\n", loc);
    if ((loc == NE_CORNER) && (loc != prev_corner)){
      current_lap_quarter++;
      turn_in_corner();
      set_pos(X_MAX-target_distance_to_wall, Y_MAX-DIST_BACKWARD_AFTER_OBSTACE);
      set_direction(-1., 0.);
      target_distance_to_wall = DIST_WALL_MEDIUM;
      prev_corner = loc;
    }else if ((loc == NW_CORNER) && (loc != prev_corner)){
      current_lap_quarter++;
      turn_in_corner();
      set_pos(DIST_BACKWARD_AFTER_OBSTACE, Y_MAX-target_distance_to_wall);
      set_direction(0., -1);
      target_distance_to_wall = DIST_WALL_MEDIUM;
      prev_corner = loc;
    }else if ((loc == SW_CORNER) && (loc != prev_corner)){
      current_lap_quarter++;
      turn_in_corner();
      set_pos(target_distance_to_wall, DIST_BACKWARD_AFTER_OBSTACE);
      set_direction(1., 0);
      target_distance_to_wall = DIST_WALL_MEDIUM;
      prev_corner = loc;
    }else if ((loc == SE_CORNER) && (loc != prev_corner)){
      current_lap_quarter++;
      if (!touch){
        turn(-90);
        set_pos(X_MAX-DIST_WALL_BIG, target_distance_to_wall);
      }else{
        turn_in_corner();
        set_pos(X_MAX-DIST_BACKWARD_AFTER_OBSTACE, target_distance_to_wall);
      }
      set_direction(0., 1);
      target_distance_to_wall = DIST_WALL_BIG;
      prev_corner = loc;
    }else{
      // Hopeless strategy that works-ish
      sense_sonar(&dist);
      if (
        (loc == E_REGION) &&
        (target_distance_to_wall >= DIST_WALL_BIG -5.)
      ){
        dir = -1; // Most likely issue
      }else if (
        (dist < 100.) || 
        (dist > 2000)
      ){
        dir = -1;
      }else{
        dir = 1;
      }
      move_straight(BACKWARD);
      Sleep(300); // At some point, should be replaced by a function using DIST_BACKWARD_AFTER_OBSTACE
      stop_motors();
      turn(-dir*45);
    }
    reset_motors();
  }
  return 0;
}

void turn_in_corner(){
    move_straight(BACKWARD);
    Sleep(300); // At some point, should be replaced by a function using DIST_BACKWARD_AFTER_OBSTACE
    stop_motors();
    turn(-90);
}

void set_pos_p1(){
  set_pos(690., 1000.);
  set_direction(0., 1.);
  starting_pos = 1;
  target_distance_to_wall = DIST_WALL_BIG;
}

void set_pos_p2(){
  set_pos(1020., 1000.);
  set_direction(0., 1.);
  starting_pos = 2;
  target_distance_to_wall = DIST_WALL_SMALL;
}

void set_pos_p3(){
  set_pos(250., 1000.);
  set_direction(0., -1.);
  starting_pos = 3;
  target_distance_to_wall = DIST_WALL_MEDIUM;
}

void set_is_alone(int is_alone_p){
  is_alone = is_alone_p;
}

int get_is_alone(){
  return is_alone;
}

int test_ignore_mesured_distance(float distance){
  float estimated_dist;
  location_t location;
  orientation_t orientation;
  (void)distance; // For now we don't use it
                  // But we will when we will introduce another bot
  location = get_location();
  orientation = get_orientation();

  estimated_dist = get_expected_distance();

  if (!is_alone && (estimated_dist >= 0.5)){
    if (estimated_dist-distance > DISTANCE_THREESHOLD){ // only if dist < estim_dist
      #ifdef DEBUF                                      // an obst cannot augment the
      printf("[d] wall too far, ignoring dist\n");      // mesured dist
      #endif
      return 1;
    }
  }

  #ifdef DEBUG
  printf("[d] difference estimation/messure: %f\n", estimated_dist-distance);
  #endif

  // If there is a plot in the region looked at
  // by the robot:
  if (
    (location == PL_1) &&
    (orientation == FACING_SOUTH)
  ){
    #ifdef DEBUG
    printf("[d] PLOT 1 in field of view, ignore dist\n");
    #endif
    return 1;
  }
  if (
    (location == PL_2) &&
    (orientation == FACING_NORTH)
  ){
    #ifdef DEBUG
    printf("[d] PLOT 1 in field of view, ignore dist\n");
    #endif
    return 1;
  }
  return 0;
}

void hook(void){
  struct timespec date;
  if (hook_frame == 0){
    display_map();
    hook_frame = 25;
  }else{
    hook_frame --;
  }

  clock_gettime(CLOCK_MONOTONIC, &date);
  #ifdef DEBUG
  printf("[d][*] lap quarter: %d\n", current_lap_quarter);
  printf("[d][*] obst_released: %d\n", obst_released);
  printf("[d][*] time diff: %f\n", timedifference_msec(date, date_last_bump));
  #endif
  if (
    !is_alone && 
    (current_lap_quarter >= 5) && 
    !obst_released && 
    (timedifference_msec(date, date_last_bump) > 1500)
  ){
    #ifdef DEBUG
    printf("[d] R4, prepare to jettison the spare part canisters.\n");
    #endif
    release_obstacle();
    clock_gettime(CLOCK_MONOTONIC, &date_obst_release);
    obst_container_in_place = 0;
    obst_released = 1;
  }

  if (
    obst_released && 
    !obst_container_in_place &&
    (timedifference_msec(date, date_obst_release) > 1500)
  ){
    #ifdef DEBUG
    printf("[d] Put the container in place.\n");
    #endif
    replace_obstacle_container();
    obst_container_in_place = 1;
  }
}

int test_stop_following(void){
  location_t loc;
  orientation_t orientation;
  float x, y;
  if (test_stop_following_touch()){
    return 1;
  }

  // Test is we are in the corner SE
  loc = get_location();
  orientation = get_orientation();
  get_pos(&x, &y);
  if (
    (loc == SE_CORNER) &&
    (orientation == FACING_EAST) &&
    (x > X_MAX - DIST_WALL_BIG)
  ){
    return 1;
  }
  return 0;
}
