#include <stdio.h>
#include <string.h>

#include "ev3.h"
#include "ev3_tacho.h"
#include "ev3_sensor.h"

#include "hardware.h"
#include "tests.h"
#include "motors.h"
#include "positionning.h"
#include "core.h"

#include "utils.h"

#define AUTHOR "Jean-Marie Mineau"
#ifndef COMMIT
  #define COMMIT "undef"
#endif

/*
 * Initialization.
 * Return 0 if the initialization succeded, 1 else.
 *
 * Implemented by Jean-Marie
 */
int init(){
  uint8_t sn;
  int init_counter;
  if (ev3_init() == -1){
    printf("Initialization failed");
    return 1;
  }
  
  init_counter = 0;
  while (ev3_tacho_init() < 1 && init_counter < 3){
    init_counter ++;
    Sleep(1000);
  }
  if (init_counter == 3){
    printf("Failled to initialize motors after 3 attempts. Abort\n");
    return 1;
  }
  reset_motors();

  init_counter = 0;
  while (ev3_sensor_init() < 1 && init_counter < 3){
    init_counter ++;
    Sleep(1000);
  }
  if (init_counter == 3){
    printf("Failled to initialize sensors after 3 attempts. Abort\n");
    return 1;
  }

  if (ev3_search_tacho_plugged_in(OBST_MOTOR,0, &sn,0)){
    set_tacho_command_inx(sn, TACHO_RESET);
    set_tacho_command_inx(sn, TACHO_HOLD);
  }else{
    printf("[-] Failed to reset objstacle motor\n");
    printf("    Tacho motors not plugged in Port %d\n", OBST_MOTOR);
    printf("    Non critical, resuming work\n");
  }
  return 0;

  set_pos_p3();
  set_is_alone(1);
  return 0;
}

int main(int argc, char * argv[]){
  int i;

  printf("***** Turing *****\n" );
  printf("version: %s\n", COMMIT);
  printf("by %s, for EURECOM OS course\n", AUTHOR);

  if (init() != 0){
    printf("panic: failed to initialize\n");
    return 1;
  }

  if (argc >= 2){
    if ((strcmp(argv[1], "--help") == 0) || strcmp(argv[1], "-h") == 0){
      printf("Usage: \n");
      printf(" - main --help / main -h\n");
      printf("       display this help message\n");
      printf(" - main test-motors\n");
      printf("       test the motors\n");
      printf(" - main test-turn\n");
      printf("       test turning\n");
      printf(" - main test-gyro\n");
      printf("       test the gyroscope\n");
      printf(" - main test-sonar\n");
      printf("       test the sonar\n");
      printf(" - main test-touch\n");
      printf("       test the touch sensor\n");
      printf(" - main test-travel-dist\n");
      printf("       test computation of the travelled distance\n");
      printf(" - main test-obst\n");
      printf("       test the release of obstacles\n");
      printf(" - main test\n");
      printf("       perform all tests\n");
      printf(" - main map\n");
      printf("       display the map\n");
      printf(" - main [--pos P1|P2|P3=P3] [--alone yes|no=yes]\n");
      printf("       --pos/-p: the starting position\n");
      printf("       --alone/-a: if the robot is alone in the arena\n");
      return 0;
    }else if (strcmp(argv[1], "test-motors") == 0){
      test_motors();
      return 0;
    }else if (strcmp(argv[1], "test-turn") == 0){
      test_turn();
      return 0;
    }else if (strcmp(argv[1], "test-gyro") == 0){
      test_gyro();
      return 0;
    }else if (strcmp(argv[1], "test-sonar") == 0){
      test_sonar();
      return 0;
    }else if (strcmp(argv[1], "test-touch") == 0){
      test_touch();
      return 0;
    }else if (strcmp(argv[1], "test-travel-dist") == 0){
      test_travel_dist();
      return 0;
    }else if (strcmp(argv[1], "test-obst") == 0){
      test_obst();
      return 0;
    }else if (strcmp(argv[1], "map") == 0){
      display_map();
      return 0;
    }else if (strcmp(argv[1], "test") == 0){
      test_motors();
      test_travel_dist();
      test_gyro();
      test_sonar();
      test_touch();
      test_obst();
      return 0;
    }else{
      for (i = 1; i<argc; i++){
        if ((strcmp(argv[i], "--pos") == 0) || (strcmp(argv[i], "-p") == 0)){
          i++;
          if (i >= argc){
            printf("Error: %s expect one argument among P1, P2, or P3\n", argv[i-1]);
            return 1;
          }
          if (strcmp(argv[i], "P1") == 0){
            set_pos_p1();
          }else if (strcmp(argv[i], "P2") == 0){
            set_pos_p2();
          }else if (strcmp(argv[i], "P3") == 0){
            set_pos_p3();
          }else{
            printf("Error: %s expect one argument among P1, P2, or P3, not %s\n", argv[i-1], argv[i]);
            return 1;
          }
        }else if ((strcmp(argv[i], "--alone") == 0) || (strcmp(argv[i], "-a") == 0)){
          i++;
          if (i >= argc){
            printf("Error: %s expect one argument among y, n, yes or no\n", argv[i-1]);
            return 1;
          }
          if ((strcmp(argv[i], "y") == 0) || (strcmp(argv[i], "yes") == 0)){
            set_is_alone(1);
          }else if ((strcmp(argv[i], "n") == 0) || (strcmp(argv[i], "no") == 0)){
            set_is_alone(0);
          }else{
            printf("Error: %s expect one argument among y, n, yes or no, not %s\n", argv[i-1], argv[i]);
            return 1;
          }
        }else{
          printf("Unknown command: %s\n", argv[i]);
          return 1;
        }
      }
    }
  }

  return run();
}
