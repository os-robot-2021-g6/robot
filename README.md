# Turing

Robot project for the OS class of 2021.

Robot name: Turing

![Turing, from 2064 Read Only Memories](./website/static/img/turing.jpg)

Website: [https://osproject.deso-palaiseau.fr/](https://osproject.deso-palaiseau.fr/)

## Members:

- jean-marie.mineau@eurecom.fr
- ~~Verneet.Singh@eurecom.fr~~
- ~~Yash.Kumar@eurecom.fr~~

All the works in this project is done by me (Jean-Marie) at the execption of the legacy branch 'main'. All the code is written by me, even when not specificaly mention. Although some functions code may be inspired from works done by other members of the previous group, the code have since be strongly eddited by me. It thouse cases, the author of the first implementation is credited in the documentation of the function, with a link to the initial source code located in the main branch.

## Setup

! The lib at https://perso.telecom-paristech.fr/apvrille/OS/docs/libev3dev-c.a is not working correctly for some reason (the port of the motors are not detected). It's better to copy the one in the robot at `/usr/local/lib/libev3dev-c.a` (`find / -name "libev3dev-c.a"` if you don't find it). I also share it at https://share.deso-palaiseau.frlibev3dev-c.a, but it may be specific to our situation.

```
git clone --recurse-submodules git@gitlab.eurecom.fr:os-robot-2021-g6/robot.git
cd robot
mkdir libraries
cd libraries
# wget https://perso.telecom-paristech.fr/apvrille/OS/docs/libev3dev-c.a
wget https://osproject.deso-palaiseau.fr/libev3dev-c.a
cd ..
sudo apt-get install gcc-arm-linux-gnueabi
```

## Build

Put the `.c` in `source`, the `.h` in `include`. To build the project, just do `make` at the root directory. `make` will automagically detect the source files.

If you need additionnal libraries, put them in `libraries` and edit the `LDFLAGS` line in the `Makefile` accordingly.

To enable debug functions/macro, use `make debug`.

## Startscripts

The binary takes some arguments from the cli, but the robot interface does not allow it, so I use scripts that run the binary with hardcoded arguments.
They are located in `startscripts` but they need to be located in the same directory has the binary on the bot.

## Ressouces

Documentation for the motors is here: https://docs.ev3dev.org/projects/lego-linux-drivers/en/ev3dev-jessie/motors.html


