# Makefile for compiling an ev3 project with the following folder structure:
#
#	this_folder/
# 		Makefile (this file)
#		ev3dev-c/
#		libraries/
#			libev3dev-c.a
#		include/
# 			*.h
#	 	source/
#			*.c
#
# make will compile every .c in source and create a executable `main` in the directory `release`

CC 		= arm-linux-gnueabi-gcc
CFLAGS 		= -O2 -g -std=gnu99 -W -Wall -Wno-comment -DCOMMIT=`git log -1 --format=\"%H\"`
INCLUDES 	= -I./ev3dev-c/source/ev3 -I./include/
LDFLAGS 	= -L./libraries -lrt -lm -lev3dev-c -lpthread
BUILD_DIR 	= ./build
SOURCE_DIR 	= ./source
OUTPUT_DIR	= ./release

CFILES		= $(wildcard $(SOURCE_DIR)/*.c)
OBJS		= $(patsubst $(SOURCE_DIR)/%.c,$(BUILD_DIR)/%.o,$(CFILES))

all: main

debug: CFLAGS += -DDEBUG
debug: main

main: ${OBJS} $(OUTPUT_DIR)
	$(CC) $(INCLUDES) $(CFLAGS) $(OBJS) $(LDFLAGS) -o $(OUTPUT_DIR)/main

$(OBJS): $(BUILD_DIR)

$(BUILD_DIR):
	mkdir $(BUILD_DIR)

$(OUTPUT_DIR):
	mkdir $(OUTPUT_DIR)

$(BUILD_DIR)/%.o: $(SOURCE_DIR)/%.c
	$(CC) $(CFLAGS) -c $(SOURCE_DIR)/$*.c $(INCLUDES) -o $(BUILD_DIR)/$*.o

clean:
	rm -f $(BUILD_DIR)/*.o
	rm $(OUTPUT_DIR)/main
