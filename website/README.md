# Website

The web site is generated using [Hugo](https://gohugo.io/). The theme used is [hello-friend](https://github.com/panr/hugo-theme-hello-friend).

To add a new post:

`hugo new posts/postname.md`

Then eddit `content/posts/postname.md` (it's in markdown) end set `draft: false`.

To test locally: `hugo server`.

To generate the files: `hugo -D`

To put in the new version online: `rsync -avz --delete public/ haskell.adh.auro.re:/var/www/osproject.deso-palaiseau.fr/`
