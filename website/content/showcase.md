---
title: "Showcase"
date: 2022-01-04T10:22:04+01:00
draft: false
---

## Positionning system

{{< video src="/vid/positionning.mp4" type="mp4" >}}

## 3 laps:

{{< video src="/vid/3-laps.mp4" type="mp4" >}}

## Feeback loops:

The first steep of Turing: a simple feedback loop:

{{< video src="/vid/feedbackloop-dist.mp4" type="mp4" >}}

A more sophisticated feedbackloop (which still need some fine tunning):

{{< video src="/vid/2nd_order_fbl.mp4" type="mp4" >}}

## Bloopers

### I lost the wall!!!

{{< video src="/vid/lost-wall.mp4" type="mp4" >}}
