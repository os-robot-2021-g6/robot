---
title: "About"
date: 2022-01-04T10:22:04+01:00
draft: false
---

This is the websit of my group for the semester project of the OS class at EURECOM. The instructions for the project are available [here](https://perso.telecom-paristech.fr/apvrille/OS/projects_fall2021.html).

Our sources are on [the gitlab of the school](https://gitlab.eurecom.fr/os-robot-2021-g6/robot), but I may keep them private for some time.

The robot is named **Turing**, after the main character of the game [2064 Read Onlt Memories](http://2064.io/).

{{< figure src="/img/turing.jpg" title="Turing from '2064 Read Onlt Memories'" >}}

# Members:

- Jean-Marie Mineau
- ~~Verneet Singh~~
- ~~Yash Kumar~~

