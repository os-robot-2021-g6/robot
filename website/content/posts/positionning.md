---
title: "Positionning"
author: Jean-Marie Mineau
date: 2022-01-25T23:20:18+01:00
---

In order to optimize the route of the robot and to make the overall robot more resilient, I added a positionning system.

The position of the robot is always available using `get_pos(&x, &y)`. In return, some bookkeeping operations have to be performed. 

Initially, I wanted to used the gyroscope to have the orientation of the robot, but the mesurements from the gyroscope were too random. However the feedback loop is a lot more reliable than expected, so I considered that the robot can only be oriented in one out of 4 directions, and the direction is manually set when turning to follow a new wall. At the moment of the turn the orientation is a verry rought estimation, the robot will automagically correct it while following the new wall. In addition, when turning, the exact position of the robot is known, because the distance to the initial wall is assured by the feedback loop, and the distance to the wall in front of the robot is assured by the touch sensor. This way, we can regularly reset the drift on the position. The rest of the time, the position of the robot is computed from the orientation of the robot, the last exact position known, and the difference of the position of the motors since the last known position.

To make the position more usable, the arena is divided into 7 zones:

```C
typedef enum location{
  NE_CORNER,
  NW_CORNER,
  SW_CORNER,
  SE_CORNER,
  PL_1,
  PL_2,
  E_REGION,
  W_REGION,
  LOCATION_ERROR
} location_t;
```

This way we can modify the behaviour of the robot depending on its location. For instance, the robot we move straight foward blindly when in `PL_1`, because we know there is an obstaccle that will modify the distance to the wall mesured by the sonar.

Other heuristics can now be implemented using the position of the robot. I wont go into details because they are mostly implemented through trials and errors an are not verry interesting, but one of those is estimating the distance to the wall that should be mesured by the sonar, and if the mesured distance is too far from the estimated one, we can assume there is another robot between the sonar and the wall, and we should not try to keep the messured distance contant.

## Demonstration

Here is a demonstration of the positionning system in action. In the video the robot only keep the position updated, it does not use it to correct the trajectory.

{{< video src="/vid/positionning.mp4" type="mp4" >}}
