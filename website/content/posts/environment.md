---
title: "Environment"
date: 2022-01-09T11:49:39+01:00
author: Jean-Marie Mineau
draft: false
---

I choosed the last setup to compile the project. The compiller is on a server (named `haskell` in the snippets) so that every one can use it without having to setup the compiler locally (even if it's irrelevant now that I am the only one in the group). The website is hosted on the same server. The code is versionned with git, and shared with EURECOM's gitlab.

## Setup

To setup the compiler and project, I ran those command (here, the OS is Debian 11 'bullseye').

```bash
sudo apt-get install gcc-arm-linux-gnueabi
git clone --recurse-submodules git@gitlab.eurecom.fr:os-robot-2021-g6/robot.git
cd robot
mkdir libraries
cd libraries
# wget https://perso.telecom-paristech.fr/apvrille/OS/docs/libev3dev-c.a
wget https://osproject.deso-palaiseau.fr/libev3dev-c.a
cd ..
sudo apt-get install gcc-arm-linux-gnueabi
```

The library available on at [https://perso.telecom-paristech.fr/apvrille/OS/docs/libev3dev-c.a](https://perso.telecom-paristech.fr/apvrille/OS/docs/libev3dev-c.a) does not seams to work. I add issues with the motors with is. Instead, I used the one available on the bot itself, at `/usr/local/lib/libev3dev-c.a` (available [here](/libev3dev-c.a)).

The project directory looks like this:

```
|-- Makefile
|-- README.md
|-- build
|   `-- *.o
|-- ev3dev-c
|   `-- *
|-- include
|   `-- *.h
|-- libraries
|   `-- libev3dev-c.a
|-- release
|   `-- main
|-- source
|   `-- *.c
`-- website
    `-- *
```

`./build` and `./release` are obviously not commited. `./include` contains the headers, `./source` contains the sources. `./ev3dev-c` is a git submodule for the robot libraries, and `./website` contains the sources of the website (it uses [hugo](https://gohugo.io/), the content is in markdown, and the compiled website is statically served with [nginx](https://nginx.org/en/)).

## Makefile

The Makefile is slightly modified:

```Makefile
CC 			= arm-linux-gnueabi-gcc
CFLAGS 		= -O2 -g -std=gnu99 -W -Wall -Wno-comment
INCLUDES 	= -I./ev3dev-c/source/ev3 -I./include/
LDFLAGS 	= -L./libraries -lrt -lm -lev3dev-c -lpthread
BUILD_DIR 	= ./build
SOURCE_DIR 	= ./source
OUTPUT_DIR	= ./release

CFILES		= $(wildcard $(SOURCE_DIR)/*.c)
OBJS		= $(patsubst $(SOURCE_DIR)/%.c,$(BUILD_DIR)/%.o,$(CFILES))

all: main

debug: CFLAGS += -DDEBUG
debug: main

main: ${OBJS} $(OUTPUT_DIR)
	$(CC) $(INCLUDES) $(CFLAGS) $(OBJS) $(LDFLAGS) -o $(OUTPUT_DIR)/main

$(OBJS): $(BUILD_DIR)

$(BUILD_DIR):
	mkdir $(BUILD_DIR)

$(OUTPUT_DIR):
	mkdir $(OUTPUT_DIR)

$(BUILD_DIR)/%.o: $(SOURCE_DIR)/%.c
	$(CC) $(CFLAGS) -c $(SOURCE_DIR)/$*.c $(INCLUDES) -o $(BUILD_DIR)/$*.o

clean:
	rm -f $(BUILD_DIR)/*.o
	rm $(OUTPUT_DIR)/main
```

This makefile detect automagically the source files in `./source`. Now I only need to modify the `Makefile` if I need to add a new library. The binaries are stored in `./build` and the final binary is put in `./release/main`. Using `make debug` instead of `make` will define the variable `DEBUG` in the preprocessor and compiles the debug features in the code delimited by `#ifdef DEBUG` and `#endif`). However, `make debug` after `make` may break, so I need to do a `make clean` each time I switch from debug mode to release mode and vice versa.

## Git

To simplify the transfer of file from local computer to the server, I added a remote to the local repository:

```bash
git remote add build haskell:~/robot/.git
```

On the remote repository, I ran to allow pushing to a non bare repository.

```bash
git config --local receive.denyCurrentBranch updateInstead
```

Now, I can push to the compil server from our local computers with `git push build`. To go further I could even add a git hook:

```sh
#!/bin/sh
cd "$GIT_DIR/.."
make
```

at `./.git/hooks/post-update` to compile the code automatically each time I push it, but during developpement, it's better to be hable to read the compiler outputs and chose if I want the debug option activated.

To test the compiled file, I just use `scp` to download it from the robot.

## Container Systemd

After some time using a server is boring. Sadly the cross-compiler packet for archlinux is mostly broken. In order to compil the project on my laptop I used systemd-nspawn, a container system using systemd (for some reason I realy don't like docker).

The first steep to make a container is create the file system. I want a debian container, so I used debootstrap to make it:

```bash
sudo debootstrap --include=systemd-container --components=main,universe bullseye compiler-ct http://deb.debian.org/debian/
```

Then I entered the container and installed a few librairies, like sudo or makefile, and created an user:

```bash
sudo systemd-nspawn -D ./compiler-ct
apt install passwd sudo makefile git 
adduser histausse
exit
```

Then I entered the container as the new user:

```bash
sudo systemd-nspawn -u histausse -D ./compiler-ct
```

And from there I cloned the repository. Because the user inside the container happened to have the same uid as my user on the host, the permissions for the files are the same. This means I can work from the host computer on the files at `./compiler-ct/home/histausse/robot/`, and in the same time, from inside the container, compile the project using the cross compiler from the debian repository.
