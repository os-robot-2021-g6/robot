---
title: "Motors"
date: 2022-01-09T14:05:14+01:00
author: Jean-Marie Mineau
draft: false
---

The documentation for the motors is available [here](https://docs.ev3dev.org/projects/lego-linux-drivers/en/ev3dev-jessie/motors.html), and the one for the library controling the motors [here](https://in4lio.github.io/ev3dev-c/ev3__tacho_8h.html).

The most interesting part about the motors is that they are asyncronous: when I send them a command, they execute it without blocking the program execution. This means I won't need to use parallelism to control the motors while checking the sensors and doing other things.

II control the motors by settings the speed (and/or other value) and a command, like this:

```C
set_tacho_speed_sp(sn, speed);
set_tacho_command_inx(sn, TACHO_RUN_FOREVER);
```

- `sn` is the squence number of the motor, retreived with `ev3_search_tacho_plugged_in(motor, 0, &sn,0)`, where `motor` is the port of the robot (eg, `66`).
- `speed` is the speed target. It must be smaller than the max speed of the motors. Usually I stick to a lower speed than the max speed because the max speed is the value for the motor without charge: I may lose in precision if I aim to high. A negative speed make the motor go backward.
- `TACHO_RUN_FOREVER` does exactly what it says it does. The unit is "tacho counts/sec", on lego it should be equal to "degrees/sec", but is it not recommended to uses it directly (cf the doc).

To get the max speed of a motor:

```C
get_tacho_max_speed(sn, &max_speed);
```

To stop the motors:

```C
set_tacho_command_inx(sn, TACHO_STOP);
```

With those primitves, I have a good control of the motors. Obviously, checking for errors is still needed and some coordination between the left and right motors is needed (cf the source code).

