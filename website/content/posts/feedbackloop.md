---
title: "Feedback Loops, or the Art of Following a Wall"
date: 2022-01-09T16:07:27+01:00
author: Jean-Marie Mineau
draft: false
---

One of the main features I need for this project is to follow a wall. I could blindly run precomputed instructions, but the erros will add over times and I would be lucky if I manage to do even one lap.

To avoid additive errors, I need to use feedback loops *(send help please :weary:)*:  I have a distance captor on the side of the robot, I will use this captore to correct the distance to the wall.

## Feedback Loop

Usually, feedback loops have three orders. They act on a value, using mesurement made on this value, the derivative of the value, and its integral (or a value related to the value it act upon):

{{< figure src="/img/feebackloop.png" title="Diagram of an usual feedback loop" >}}

On my feedback loop, I will use the distance to the wall as \\(e(t)\\), and act on the speed of the motor as \\(u(t)\\). To simplify things, I will set \\(K_i = 0\\) (feedback loops are complicated enought with 2 orders). The linear order is usefull to keep the distance constant in a straight line and to correct errors on the long terms. The derivative order is usefull to keep the direction parallel to the wall will taking a turn.

## Linear Feedback

For a first version, I set \\(K_d = 0\\) too. This feedback loop is very good at following a straight line, but don't do well in curves:

{{< video src="/vid/linear_fbl.mp4" type="mp4" >}}

This implementation should be enough if I manage to do a blind U turn at the end of the central fence. However, if I am not alligned enough at the end of the turn, this feedback loop won't be enought.

## Derivative Feedback

Now, I set \\(K_p = 0\\) instead of \\(K_d\\). The feedback loop has no memory of the distance to maintain, but it reacts better when taking curves. It is less stable thant the linear loop, and error are added at each change of direction: it only keep the direction parallel to  the wall.

{{< video src="/vid/derivative_fbl.mp4" type="mp4" >}}

One thing to keep in mind when implementing a derivative mesurement is to keep tract of time with a system clock, and not by rely on the time in the `delay()` calls: the OS may let us wait longer, and the other operations between two `delay()` may not be negligeable compared to the duration of the wait.

## 2n order feedback loop

Now, I can mix the two together:

{{< video src="/vid/2nd_order_fbl.mp4" type="mp4" >}}

To add more control on the computation, I added a some non-linearity to the equation with a linear and a derivative looseness factor which remove the linear (derivative) therme is the distance delta (derivative) is smaller than the looseness factor. This way I can benefit the stability of the linear feedback loop in straight lines will keeping the reactivity of the linear feedback loop in curves. (this feature is not used in the video demonstration)

Now all I have to do (for the feedback loop at least) is to fine-tune the different parameters, and it should work, probably...

{{< figure src="/img/it-works.gif" title="perfectly working system" >}}
