---
title: "Architecture of the Robot"
author: Jean-Marie Mineau
date: 2022-01-25T22:42:51+01:00
---

The main specificity of my robot is the sonar on the side. When most groups decided to put it in the front, I wanted to use the distance to the wall on the side to do a feedback loop.

![Picture of the Sonar](/img/archi/img_1.jpg#center)

Contrary to the robots in the minestorm sheets, I also decided to the motors around the ev3 brick instead of under the brick. This is just because the position of the sonar is exactly at the right height this way: just low enought to be at the level of the walls/fences, and just high enough to be over the balls.

![Picture of the other side of Turing](/img/archi/img_0.jpg#center)

![Picture of the back of Turing](/img/archi/img_3.jpg#center)

![Picture of underneath Turing](/img/archi/img_4.jpg#center)

To counterbalance the absence of sonar in the front, I put a touch sensor here. Initially I wanted to but some sort of bumber to detect a contact anywhere in the front, but the leverage prevented the sensor from activating, so I just kept a punctual sensor.

![Picture of the front of Turing](/img/archi/img_2.jpg#center)

Lastly I added container on the top of the robot to store obstacles to release during the run. Sadly I didn't had the time to add some speaker to play an appropriate Star-Wars reference, but the spirit is here.

![Appropriate reference :)](/img/jettison_spare_parts.gif#center)

![Container for abostacles](/img/archi/img_5.jpg#center)

![The other side of the container](/img/archi/img_6.jpg#center)

My robot don't use any other sensors. Initialy I tried using a gyroscope, but the value were too unreliable so I removed it. An interesting point is that I don't use a magnetic sensor, but other robots do... Interesting isn't it? It would be a shame is someone dumped some magnet in the arena...

![Are those magnet?](/img/archi/img_7.jpg#center)

![No idea what's inside this ball](/img/archi/img_8.jpg#center)

I choosed aluminum for my obstacle because I hope its surface will have strange effects on the sonars. And my sonard is high enought to not detecting it.
