---
title: "The Hardness of Group Work"
author: Jean-Marie Mineau
date: 2022-01-27T23:20:09+01:00
---

The main failure of this project was without a doubt the group work, considering that I ended up in a group of one.

## Finding a group

Not beeing particularly social, I did know anyone to form a group to work on this projet. Eventually, I was assigned to a group.

## Group work

At first, the group work have been distributed like this: I worked on the development environment while the two other member worked on the robot (It was more convenient as they lived close to each other). It worked a the beginning, and approximately at the same time, we had linux flashed on the ev3 brick, a base robot, and a development environnement ready.

At this point the project stagnated a little. Yash and Verneet didn't showed the lab sessions, and eventually in total I had access to the robot for only one afternoon, just enough to test that the cross compiler actually worked. After that Yash kepted the robot for the holidays. Between the difficulties of working remotly on the robot, other homeworks and the holidays, I didn't worked on the project. At this point the 'group' work should have already worried me: even before the vacation the discutions about the project were going nowhere, I barely saw my teammates, the only time we managed to see each other to work on the project, Yash was the only one to show up, and he spend most of the time trying to make me do his lab homeworks instead of working on the project. 

At the end of the holidays, I started refactoring the snippets of code Yash commited to the git (without using the development environnement I worked on, another red flag I should have noticed). Thursday, Yash and Verneet again didn't show at the lab session, and when they arrived in the afternoon, the more productive result of the session was me getting the robot home to work on it. After a few days working on the robot I managed to put together a code base for the robot. At this point we where a few days away from the first tests session. A gave a list a features and functions that needed to be implemented to the rest of the group, like a system to release obstacles. Unfortunately I spent to much time working on the robot and I had to catch up some homeworks, so with Yash we decided that I let the robot at the reception of EURECOM so that he recover it and work on it.

The 13 of jan, I found the robot still at the reception, and no commits on the gitlab, and after the tests, Yash and Verneet told me that they wanted to remodel the robot, moving the sonar sensor in the front "like everyone else", rendering a good part of my code base useless. I firmly refused, on the account that recoding the code base 2 weeks before the deadline was not a good idea, and considering the poor quality of the 3 commit of Yash (and the absence of commit from Verneet), I was most likely the one that would have to code the most part of the new version. By the end of the lab session, Yash and Verneet had spend the whole time reading the instruction of the project, without coding or building anything on the robot. At this point it was clear that the group was not working **at all**.

## The breakup

Considering the obvious issues within the group, the professor sent an email reminding that the grades are individual, giving some advice on how to work in group, and explaining that if working as a group was not possible, breaking the group was a possibility. At this point, I was seriously considering breaking the group myself, but I wanted to talk about it with the group. But before I got the change, Yash was out of the chat group, and by the end of the day I received an email from the teacher announcing me that Yash and Verneet decided to split up with me.

## A group of one

I will admit that even if I was seriously considering breaking the group myself, beeing kicked out without any communication got me a little angry. After that, I admit I found kicking them out of the gitlab quite satisfactory, although a bit childish.

The main consequence of spliting the group was that I had to get and build a new robot. It resulted on more work, once done, I found it a lot easier to work. Considering the situation in the group, being alone did not change the workload a lot. In fact, not having to explain and justify my choices allowed me to work faster (like they say, 'What one programmer can do in one month, two programmers can do in two months'). 

## Conclusion

At the end, even if the project lacked some features that would have been easily developed in parallel in a group, the overall result was satifying. I am fearly sur that working with a good groups would have improved the result, but I am still wondering how staying in my initial group would have affected the result.

In hindsight, the issues with the group were obvious from almost the begining, and I should have acted on it. Abviously the lake of motivation of the other members is a cause of those issue, **yet**, I belive I am also a lot to blame. My social ineptness did not help at all the situation. The situation required a lot more communication from both side, and I spectacularly failled to initiat a meaningfull dialog or regular communication during the project.

Even if the technical result of the project suits me, the group aspect of it is a complete failure and it is quite disapointing.
