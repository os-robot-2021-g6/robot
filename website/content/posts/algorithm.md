---
title: "Algorithm"
author: Jean-Marie Mineau
date: 2022-01-28T17:37:45+01:00
---

I won't enter in the details of the implementation here. There is an article aboutthe implementation of the feedback loop used to follow the wall [here](/posts/feedbackloop), and some details about the control of the motors [here](/posts/motors). There is nothing much to tell about the fine details of the implementation, and the [code](https://gitlab.eurecom.fr/os-robot-2021-g6/robot) should be readable enought if needed.

## Strategy

Having already worked on [similar robot before](#unrelated), I knew from the start that drift would be my worst enemy. So I decided build a robot that would minimize the drift by design. The first step was to list the sensor available, and whether the value given by them was subject to drift or not. The touch sensor, the sonar and the compas were not affected by drift, while the gyroscope and the tachometers of the motors were. Unfortunately the compas was not verry usable in this context due to the disturbed magnetic field in the room. That was an issue because loosing the compas meant loosing a drift-free mesurement of the orientation of the robot. On the other hand, the contact sensor and the distance sensor where redundant if placed in the front of the robot like most of the people/exemple in the manual did. This, combined with some old memory of engineering classes, lead me to the idea of using a feedback loop. Because the walls are (mostly) straight, keeping the distance to the wall constant would fix the orientation of the robot. I won't have a mesurement of the orientation of the robot, *but* the robot will correct its orientation to match the theoretical one by itself. In addition, the sonar will still give me one of the two value of the coordinates of the robot. Using a feedback loop this way allow to get two information drift-free with only one captor: that sounds like an optimal usage of the sensor. Now, I still need the other coordinate of the robot, wich can be computed using the motors' tachometers. However, this mesurement is subject to the drift, and this go against the initial idea. Whitout a second distance sensor on the front, it is not possible to fix this issue completely. The next best thing is recalibrating this coordinate regularly, and it happends that if the feedback loop works as intended, the robot will always collide with the wall in front of it at the same location. With the touch sensor, this collision can be detected, meaning I can link an absolute position to the input of a sensor that don't drift 4 times in one lap.

This strategy can be resumed by this algorithm:

```
DO 4*NB_LAP+1 TIMES:
  FOLLOW_THE_WALL_UNTIL_COLLISION()
  CALIBRATE_POSITION()
  TURN(90°)
DONE
```

Because we don't carry any drift between each iteration, from the moment one iteration works, the robot can turn indefinitly.

## Improuvement

This first strategy works verry well when the robot is alone. In fact, It works ever better than expected: when the obstacle where added, the robot managed to recover enough to finish the iteration, and thanks to the absene of drift, the robot could continue it course without any probleme. However, this is still suboptimal, and any modification to the arena (for instance, the presence of another robot), could affect in unexpected ways. The worst beeing the robot recalibrating the wrong position, leading to the robot making a U-turn or stucking it against the fence.

Until now, we don't use the estimated position of the robot. Our next step is take the estimated position into account. To do so, we will first replace `FOLLOW_THE_WALL_UNTIL_COLLISION()` by `FOLLOW_THE_WALL(distance, TEST_STOP_FOLLOWING, TEST_IGNORE_SONAR, HOOK)`. `distance` is the distance we want to keep between the wall and the robot, `TEST_STOP_FOLLOWING()` is a **non blocking** function, called regularly inside `FOLLOW_THE_WALL(...)`. When `TEST_STOP_FOLLOWING()` is true, `FOLLOW_THE_WALL(...)` exit and the robot stop. This function uses the estimated position of the robot, the value of the sensors and other global variables to make the decision. `TEST_IGNORE_SONAR()` is also **non blocking** and called regularly inside `FOLLOW_THE_WALL()`. While its value is true, the robot will stop following the wall and move straight foward. This function is used to estimate if there is an obstacle between the sonar and the wall. For instance, if the robot is competing against another robot and the distance messured by the sonard is a lot smaller than it should be considering the position of the robot, there is probably something between the robot and the wall (like the other robot). More simple, if the robot is near one of the obstacle of the arena, the robot should ignore the value of the sonar the time to pass by. The last function, `HOOK()`, is also called regularly inside `FOLLOW_THE_WALL(...)`. This function is used to run periodically some instructions. For instane, it print a map with the position of the robot every 25 calls. A planned use for this function was to integrate the gyroscope to the estimation of the position, allowing a more precise positionning of the robot. Sadly I did not had time to implement this use. The last use of `HOOK()` is releasing the obstacles: when certain set of conditions (taking into account the position of the robot, the number of laps made, some timing, ect...) is true, `HOOK()` calls the (non blocking) function that release the obstacles. They could be many more uses for `HOOK`, for instance it could "pre-empt" `FOLLOW_THE_WALL(...)` by running some commands related to the motors in a blocking way (This is a image, in reality all the code is executed sequentially). In addition, the consistency of the position is checked at each iteration of the main loop. If the robot stopped following the wall whitout being approximatly in the right corner, there is an issue.

```
DO 4*NB_LAP+1 TIMES:
  FOLLOW_THE_WALL(
    distance,
    TEST_STOP_FOLLOWING,
    TEST_IGNORE_SONAR,
    HOOK
  )
  IF ESTIMATED_CORNER() == NEXT(last_corner):
    CALIBRATE_POSITION(ESTIMATED_CORNER())
    last_corner = ESTIMATED_CORNER()
    TURN(90°)
    distance = F(ESTIMATED_CORNER(), IS_ALONE(), ...)
  ELSE
    RECOVERY()
  ENDIF
DONE
```

`F` and `RECOVERY()` are function chosen empirically, throught trial and error. They are not verry sophisticated nor interesting to look at. Especially, `RECOVERY()` is solicitated a lot when another robot or an obstacle is *in front* of the robot, instead of *beside* the robot. It would had been good to spend more time on it and find a proper method to detect the reason the robot stopped following the wall. Unfortunatly, I an out of time before. Another ameliration would have been a frequency analysis of the stops of `FOLLOW_THE_WALL(...)`: to much time between two stops would means the robot is suck without trigering the touch sensor, to many stops in a short time would mean that the robot is stuck against a wall or another robot.

## Results

My strategy showed very good results against the clock. While my robot ignore the drift and go at full speed, the other robots had to compensate the drift through sophisticated recovery strategy most of the time.

In return, my poor recovery strategy was paid for in the pursuit race: My robot was much faster than the average competitor, and it was unable to recover after a frontal collition with the other robot.

The result during a side by side race was better: my algorithm is better suited for adversary on the side, and the speed adventage allowed my robot to avoid interracting to much with the other robot after the start. Alas, if for some reason my robot needed to recover from a frontal interference, it was almost certain to lose the race.

As a side note, during the competition, `TEST_IGNORE_SONAR` was poorly configured and trigered too easily, resulting in the robot beeing unable to follow the wall on the EAST of the arena.

---

## Unrelated

Yes, I have worked before with lego MINDSTRORMS, in 8th grade I participated in a robot competition with other classmates:

![The first robot I worked on](/img/verry_old_picture.JPG#center)

![Another picture of the robot](/img/verry_old_picture2.JPG#center)

(Obviously, the competition was an epic faillure, but we had a lot of fun working on it)
